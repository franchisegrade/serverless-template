import {
  rollbar
} from '../../../shared-libs'

export const handler = rollbar.lambdaHandler(async (event) => {
  console.log(event)

  return {
    statusCode: 200,
    name: 'name'
  }
})